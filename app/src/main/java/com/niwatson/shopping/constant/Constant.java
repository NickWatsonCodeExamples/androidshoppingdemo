package com.niwatson.shopping.constant;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.niwatson.shopping.model.Product;

public final class Constant {
    public static final List<Integer> QUANTITY_LIST = new ArrayList<Integer>();

    static {
        for (int i = 1; i < 11; i++) QUANTITY_LIST.add(i);
    }

    public static final Product PRODUCT1 = new Product(1, "Apple iPhone XS Max", BigDecimal.valueOf(1099.00), "The XS Max has the largest ever display on an iPhone and the most accurate colours, HDR and true blacks, thanks to the industry-leading custom OLED display.", "iphonexsmaxgold");
    public static final Product PRODUCT2 = new Product(2, "Apple iPhone XR", BigDecimal.valueOf(749.00), "The iPhone XR - brilliant. In every way. Discover the most advanced LCD screen and most powerful processor in Apple's iPhone XR.", "iphonexr");
    public static final Product PRODUCT3 = new Product(3, "Apple iPhone XS", BigDecimal.valueOf(999.00), "The Apple iPhone XS gives you the most accurate colours, HDR and true blacks, thanks to the industry-leading custom OLED display.", "iphonexsmaxgold");
    public static final Product PRODUCT4 = new Product(4, "Apple iPhone X", BigDecimal.valueOf(769.00), "With Apple iPhone X, the device is the display. An all-new 5.8-inch Super Retina display employs new techniques and technology to precisely follow the curves of the design. The OLED screen delivers accurate, stunning colours, true blacks, high brightness, and a 1,000,000 to 1 contrast ratio.", "iphonex");

    public static final List<Product> PRODUCT_LIST = new ArrayList<Product>();

    static {
        PRODUCT_LIST.add(PRODUCT1);
        PRODUCT_LIST.add(PRODUCT2);
        PRODUCT_LIST.add(PRODUCT3);
        PRODUCT_LIST.add(PRODUCT4);
    }

    public static final String CURRENCY = "£";
}
